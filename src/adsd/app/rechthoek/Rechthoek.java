package adsd.app.rechthoek;

public class Rechthoek
{
    // attributes
    private double x = 0.0;
    private double y = 0.0;
    private String omschrijving = "";

    // operations

    // Constructor
    public Rechthoek(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    // Methods (functions)

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public void setOmschrijving(String omschrijving)
    {
        this.omschrijving = omschrijving;
    }

    public String getOmschrijving()
    {
        return omschrijving;
    }

    public double berekenOppervlakte()
    {
        // Calculate and return oppervlakte
        return (x * y);
    }
}