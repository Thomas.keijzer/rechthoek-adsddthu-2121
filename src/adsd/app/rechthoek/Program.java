package adsd.app.rechthoek;

public class Program
{
    Rechthoek rechthoek1;
    Rechthoek rechthoek2;
    Rechthoek rechthoek3;

    /**
     * Constructor of this class
     */
    public Program()
    {
        rechthoek1 = new Rechthoek(10, 5);
        rechthoek1.setOmschrijving("Rechthoek 1");

        rechthoek2 = new Rechthoek(7, 18);
        rechthoek2.setOmschrijving("Rechthoek 2");

        rechthoek3 = new Rechthoek(7, 3);
        rechthoek3.setOmschrijving("Rechthoek 3");

        // Print gegevens
        printRechthoek(rechthoek1);
        printRechthoek(rechthoek2);
        printRechthoek(rechthoek3);
    }

    /**
     * Print rechthoek method
     * @param huidigeRechthoek
     */
    public void printRechthoek(Rechthoek huidigeRechthoek)
    {
        System.out.println("------");
        System.out.println(huidigeRechthoek.getOmschrijving() + ": " +
                            huidigeRechthoek.getX() + " bij " +
                                huidigeRechthoek.getY());
    }

    /**
     * Start program
     */
    public static void main(String[] args) {
        Program mainClass = new Program();
    }
}
